import string
from src.generator import *


def gen_all_license_plates():
    """
    --- Memory intensive --- Require 4.11 GB of free space
    Produce the list of all the italian modern license plates
    """
    #
    AZ = list(string.ascii_uppercase)
    num = list(string.digits)
    fix = list(generate_word(AZ, 2))  # necessary to reuse it in two places
    body = generate_word(num, 3)
    license_plate = list(combine_words(fix, body, fix))
    save_words('../data/license.txt', license_plate)


def gen_license_plate(prefix='CH'):
    """
    Generate the list of the license plate starting with a given prefix
    """
    assert len(prefix) == 2
    AZ = list(string.ascii_uppercase)
    num = list(string.digits)
    suffix = generate_word(AZ, 2)
    body = generate_word(num, 3)
    license_plate = combine_words(body, suffix)
    pattern = prefix + '%s'
    save_words('../data/%s_license.txt' % prefix, license_plate, pattern)


if __name__ == '__main__':
    gen_license_plate()
