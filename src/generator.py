from typing import List, Tuple, Iterable
from itertools import product
import string


def ascii_range(min_chr: str, max_chr: str) -> (int, int):
    if not (isinstance(min_chr, str) and isinstance(max_chr, str)):
        raise TypeError("Error: min and max should be strings.")
    if not (len(min_chr) == 1 and len(max_chr) == 1):
        raise ValueError("Error: the length of the strings should be 1.")
    return ord_range(ord(min_chr), ord(max_chr) + 1)


def ord_range(min_ord: int, max_ord: int) -> (int, int):
    if not (isinstance(min_ord, int) and isinstance(max_ord, int)):
        raise TypeError("Error: min and max should be integers.")
    if min_ord < 0 or max_ord > 127 or min_ord > max_ord:
        raise ValueError("Error: min should be not greater than max, and both of them should assume values in [0,127].")
    return min_ord, max_ord


def generate_alphabet(ranges: List[Tuple[int, int]]) -> Iterable[str]:
    """
    Generate every char in the given ranges
    """
    # there's a bug on python 3.7 about 'yield' in a list comprehension
    for r in ranges:
        for code in range(r[0], r[1]):
            yield chr(code)


def generate_word(alphabet: List[str], length: int) -> Iterable[str]:
    """
    Generate all the words of the provided length with the provided alphabet (list of symbols)
    """
    for t in product(alphabet, repeat=length):
        yield ''.join(t)


def combine_words(*words: Iterable[str]) -> Iterable[str]:
    """
    Concatenate N components into one word
    """
    for w in product(*words):
        yield ''.join(w)


def save_words(filename: str, gen: Iterable[str], pattern: str = '%s', ) -> None:
    """
    Save the words on a filename.
    You can modify the pattern string to set up a common prefix and/or suffix to all the elements
    """
    with open(filename, "w") as f:
        for w in gen:
            f.write(pattern % w + '\n')