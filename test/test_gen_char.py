import sys
import pytest
import numpy as np
import string

sys.path.append('..')
from src.generator import ord_range, ascii_range, generate_word, combine_words, generate_alphabet, save_words


def test_ord_range():
    assert ord_range(3, 4) == (3, 4)
    assert ord_range(54, 120) == (54, 120)
    with pytest.raises(ValueError, match=r".*values.*"):
        assert ord_range(-1, 40)
        assert ord_range(-120, -2)
    with pytest.raises(TypeError, match=r".*integers.*"):
        assert ord_range(5.0, 120.0)
        assert ord_range('a', 212)


def test_generate_alphabet():
    AZ = list(generate_alphabet([ascii_range('A', 'Z')]))
    AZ2 = list(string.ascii_uppercase)
    assert AZ == AZ2
    az = list(generate_alphabet([ascii_range('a', 'z')]))
    az2 = list(string.ascii_lowercase)
    assert az == az2
    num = list(generate_alphabet([ascii_range('0', '9')]))
    num2 = list(string.digits)
    assert num == num2


def test_generate_word():
    ad = ascii_range('a', 'd')
    NW = ascii_range('N', 'W')
    alpha1 = list(generate_alphabet([ad, NW]))
    alpha2 = list(generate_alphabet([ascii_range('0', '3')]))
    prefix = generate_word(alphabet=alpha1, length=2)
    suffix = generate_word(alphabet=alpha2, length=2)
    final = list(combine_words(prefix, suffix))  # necessary if i want to reuse it
    for i, word in enumerate(final):
        # print("%04d: %s" % (i, word))
        pass
    # ---length check---
    n = np.array([len(alpha1), len(alpha2)])
    l = np.array([2, 2])
    n = np.power(n, l)
    assert (i + 1) == n[0] * n[1]
    save_words('../data/out.txt', final)
